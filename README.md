# Ansible | Playbook - deploiement d'un conteneur httpd via Ansible

_______

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang"> 

> **Carlin FONGANG**  | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Contexte
Nous souhaitons préparer un environnement de déploiement, rédiger le playbook nécessaire et valider son exécution, garantissant ainsi que le serveur web Apache fonctionne correctement et soit accessible de l'extérieur sur le port 80. Ce laboratoire nous permettra d'automatiser la tâche de déploiement du serveur Apache avec Ansible, tout en améliorant l'efficacité et la fiabilité des déploiements.

## Objectifs

Dans ce laboratoire :

- Nous utiliserons un cluster de deux instances : une instance Ansible et une instance cliente.

- Nous créerons un fichier d'inventaire nommé `hosts.yml` (au format YAML) contenant un groupe `prod` avec comme seul membre notre client.

- Nous créerons un dossier `group_vars` qui contiendra un fichier nommé `prod`. Ce fichier contiendra les informations de connexion à utiliser par Ansible (identifiants).

- Nous rédigerons un playbook nommé `deploy.yml` permettant de déployer Apache à l'aide de Docker sur le client. Nous utiliserons comme image Docker `httpd`, et le port exposé à l'extérieur sera le 80.

- Nous vérifierons la syntaxe du playbook avec la commande `ansible-lint`.

- Nous nous assurerons qu'après l'exécution du playbook, le site par défaut d'Apache est bien accessible sur le port 80.

- Nous explorerons les informations de débogage d'Ansible.

## Prérequis
Disposer de deux machines avec Ubuntu déjà installé.

Dans notre cas, nous allons provisionner des instances EC2 s'exécutant sous Ubuntu via AWS, sur lesquelles nous effectuerons toutes nos configurations.

Documentation complémentaire :

[Documentation Ansible](https://docs.ansible.com/ansible/latest/index.html)

[Lancer une instance EC2 sur AWS à l'aide de Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws)

## 1. Création des instances hôte et client

[Lancer une instance EC2 sur AWS à l'aide de Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws)

## 2. Création du fichier d'inventaire `hosts.yml`

Nous créerons un fichier d'inventaire nommé `hosts.yml` (au format YAML) contenant un groupe `prod` avec comme seul membre notre client.

Une fois connecté à l'instance hôte, nous créerons un dossier projet et nous positionnerons dans le répertoire de ce projet :

```bash
ssh -i devops-aCD.pem ubuntu@public_ip_ansible
mkdir project
cd project/
nano hosts.yml
```

Contenu du `hosts.yml` :

```yaml                                                       
prod:
  hosts:
    srv_prod:
      ansible_host: 44.221.102.184
```

Ensuite, nous copierons la paire de clés SSH de l'instance cliente sur l'instance hôte ansible :

```bash 
mkdir .secret
touch .secret/devops-aCD.pem
```

Nous collerons le contenu de la paire de clés SSH générée dans ce fichier, enregistrerons et appliquerons les autorisations nécessaires pour l'exécution de ce dernier :

```bash 
chmod 600 .secret/devops-aCD.pem
```

>![alt text](img/image.png)


## 3. Création du dossier **group_vars** et du fichier **prod.yml**

```bash
mkdir group_vars
nano group_vars/prod.yml
```
Dans le fichier ouvert, nous allons rajouter :

```bash
---
ansible_user: ubuntu
ansible_ssh_private_key_file: ".secret/devops-aCD.pem"
```

## 4. Création du playbook **deploy.yml**

```bash
nano deploy.yml
```

Contenu du playbook :

```bash
---
- name: "Installation d'Apache à l'aide de Docker"
  hosts: prod
  become: true
  pre_tasks:
    - name: Install python3-pip
      ansible.builtin.apt:
        name: python3-pip
        state: present
        update_cache: true
      when: ansible_os_family == "Debian"

    - name: Install Docker prerequisites
      ansible.builtin.apt:
        name:
          - apt-transport-https
          - ca-certificates
          - curl
          - gnupg-agent
          - software-properties-common
        state: present
      when: ansible_os_family == "Debian"

    - name: Add Docker's official GPG key
      ansible.builtin.apt_key:
        url: https://download.docker.com/linux/ubuntu/gpg
        state: present
      when: ansible_os_family == "Debian"

    - name: Set up the stable Docker repository
      ansible.builtin.apt_repository:
        repo: deb [arch=amd64] https://download.docker.com/linux/ubuntu {{ ansible_distribution_release }} stable
        state: present
      when: ansible_os_family == "Debian"

    - name: Install Docker Engine
      ansible.builtin.apt:
        name: docker-ce
        state: present
        update_cache: true
      when: ansible_os_family == "Debian"

    - name: Install Docker Python SDK
      ansible.builtin.pip:
        name: docker-py
        executable: pip3
  tasks:
    - name: Create Apache container
      community.docker.docker_container:
        name: apache_aCD
        image: httpd
        ports:
          - "80:80"
```

Sauvegarder et enregistrer le fichier.

>![alt text](img/image-1.png)

## 5. Vérification de la syntaxe du playbook **deploy.yml**

Installation de python-pip :

```bash
sudo apt install python3-pip
```

Installation de Ansible-lint :

```bash
sudo pip install ansible-lint
```

>![alt text](img/image-2.png)
*Ansible-lint est bien installé.*

Cet outil permet de vérifier la syntaxe de nos playbooks Ansible.

Pour vérifier la syntaxe du playbook **deploy.yml**, entrez la commande suivante :

```bash
ansible-lint deploy.yml
```

>![alt text](img/image-3.png)
*Sortie console de la commande **ansible-lint**.*

## 6. Exécution du playbook deploy.yml

```bash
ansible-playbook -i hosts.yml deploy.yml
```

>![alt text](img/image-4.png)
*Sortie console de l'exécution du playbook.*

### Vérification de l'instance cliente

```bash
ssh -i devops-aCD.pem ubuntu@public_ip_client
sudo usermod -aG docker $USER
logout
ssh -i devops-aCD.pem ubuntu@public_ip_client
docker ps -a && docker inspect apache_aCD
```

>![alt text](img/image-5.png)
*Docker est installé et le conteneur httpd est en cours*

### Vérification de la disponibilité du serveur web Apache

>![alt text](img/image-6.png)
*Le serveur web Apache est bien accessible.*

### Arborescence 

>![alt text](img/image-7.png)

## 7. Configuration de ansible.cfg

Le fichier **ansible.cfg** est le fichier de configuration pour Ansible. Il  permet de définir des paramètres et des comportements par défaut pour des exécutions sous Ansible, ce qui peut rendre la gestion de d'un projets beaucoup plus simple et plus centralisée, dans notre cas, nous allons activer l'option **host_key_checking** à **False**, créer un fichier **ansible.cfg** à la racine du projet et y ajouter la ligne suivante :


```bash
[defaults]
host_key_checking = False
remote_user = ubuntu
private_key_file = .secret/devops-aCD.pem
inventory = hosts.yml
```
Ceci permettra de s'authentifier systhématiquement auprès des instances clientes, et dans ce cas, l'on aura plus besoin des variables **"ansible_user & ansible_ssh_private_key_file"** contenu dans le group_vars/prod.yml
